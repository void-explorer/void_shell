.PHONY: all
all: demo

.PHONY: demo
demo:
	@cmake -DCMAKE_BINARY_DIR=./ -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug -H./ -B./build -GNinja
	@cmake --build ./build
	@cp build/compile_commands.json ./compile_commands.json

.PHONY: run
run:
	@build/demo/void_shell_demo

.PHONY: test
test:
	@ceedling gcov:all utils:gcov

.PHONY: clean
clean:
	@rm -rf build/
