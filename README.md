# Void Shell

## Introduction

Void Shell is an open source, lightweight, statically allocated shell.
It can be used in both desktop applications as well as embedded systems,
although it is tailored for lightweight embedded processors.
The shell is designed to make creating development commands as easy as possible.
It supports command completion, command history,
and a variety of utility functions which simplify terminal interaction.

## Library Design

The library is comprised of three components:

* the shell itself
* the command processor
* utility functions which are useful for terminal interaction

```mermaid
sequenceDiagram
    participant A as Application
    participant B as Shell
    participant C as Command Processor
    A->>B: Configure input and output functions
    A->>C: Register commands
    loop Application processing
    A->>+B: User character input
    B->>+C: Command string
    C->>-B: Command output
    B->>-A: Terminal output
    end
```
