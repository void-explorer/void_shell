FROM ubuntu:latest

RUN export DEBIAN_FRONTEND=noninteractive && \
    apt-get update && \
    apt-get -y install \
    build-essential \
    cmake \
    curl \
    gcovr \
    git \
    make \
    ninja-build \
    python3-pip \
    ruby \
    openjdk-11-jdk \
    unzip \
    valgrind 

RUN gem install bundler ceedling

RUN pip3 install gcovr

# Download sonar-scanner
RUN curl -sSLo ./sonar-scanner.zip 'https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.7.0.2747-linux.zip' &&\
    unzip -o sonar-scanner.zip && \
    mv sonar-scanner-4.7.0.2747-linux sonar-scanner

# Download build-wrapper
RUN curl -sSLo ./build-wrapper-linux-x86.zip "https://sonarcloud.io/static/cpp/build-wrapper-linux-x86.zip" && \
    unzip -oj build-wrapper-linux-x86.zip -d ./build-wrapper

ENV PATH "/build-wrapper:$PATH"
##
## Add base project path to $PATH (for help scripts, etc.)
##

ENV PATH "/project:$PATH"

# Create empty project directory (to be mapped by source code volume)
WORKDIR /project

# When the container launches, run a shell that launches in WORKDIR
CMD ["/bin/sh"]